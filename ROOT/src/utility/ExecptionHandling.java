package utility;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.ServletActionContext;
import net.sf.json.JSONObject;

public class ExecptionHandling {
	
	public static void exceptionHandling(){
		
		
	    
		try {
			JSONObject ErrorJson = new JSONObject();
			ErrorJson.put(Constants.STATUS, "ErrorOccurs");
			
			
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
		    PrintWriter writer = response.getWriter();
		     
		    writer.write(ErrorJson.toString());
			
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
}