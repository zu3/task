package utility;

public class Constants {
	
	public static enum HTTPMETHODS { POST, PUT, GET, DELETE}  
	
	public enum TASK_PARAMETERS { taskId, taskName, taskDescription, taskStatus} 
	
	public static final String STATUS    = "STATUS";
	
	public static final String SUCCESS   = "SUCCESS";
	
	public static final String FAILURE   = "FAILURE";
	
	public static final String POST      = "POST";
	
	public static final String GET       = "GET";
	
	public static final String PUT       = "PUT";
	
	public static final String DELETE    = "DELETE";
	
}