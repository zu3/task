package board;
import org.apache.struts2.ServletActionContext;
import board.Board;
import utility.Constants;
import utility.ExecptionHandling;

public class BoardSeparation {

	//instance variabes.
    String boardid;
    String boardname;
    String boardtime;
	
    
    //getter and setter methods.
	public String getBoardid() {
		return boardid;
	}
	public void setBoardid(String boardid) {
		this.boardid = boardid;
	}
	public String getBoardname() {
		return boardname;
	}
	public void setBoardname(String boardname) {
		this.boardname = boardname;
	}
	public String getBoardtime() {
		return boardtime;
	}
	public void setBoardtime(String boardtime) {
		this.boardtime = boardtime;
	}

	
	public void boardSeparation(){
		
		//Httpmethods variable storing the request method type.
		String Httpmethods = ServletActionContext.getRequest().getMethod();
		
	    Board board=new Board();
	    
	    //Handling the Exeception.
	    try{
			    switch (Httpmethods) {
			    
					case Constants.POST:
			
						board.setBoardname(boardname);
						board.addBoard();	
						break;
						
					case Constants.PUT:
						board.setBoardid(boardid);
						board.setBoardname(boardname);
						board.updateBoard();	
						break;
					
					case Constants.DELETE:
						board.setBoardid(boardid);
						board.deleteBoard();
						break;
		
					default:
						ExecptionHandling.exceptionHandling();
						break;
			  }
		}
	    catch (Exception e) {
			
	    	ExecptionHandling.exceptionHandling();
		}
		
	}
}
	
