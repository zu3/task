package board;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.ServletActionContext;
import org.json.JSONException;
import org.json.JSONObject;
import dbActions.ManageBoardActions;
import utility.Constants;

public class Board {
	
		//instance variables.
		String boardid;
	    String boardname;
	    String boardtime;
		String status;
		
		//getter and setter methods.
		public String getBoardid() {
			return boardid;
		}
		public void setBoardid(String boardid) {
			this.boardid = boardid;
		}
		public String getBoardname() {
			return boardname;
		}
		public void setBoardname(String boardname) {
			this.boardname = boardname;
		}
		public String getBoardtime() {
			return boardtime;
		}
		public void setBoardtime(String boardtime) {
			this.boardtime = boardtime;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}

		//addboard method for add a new board into database.
		public void addBoard() throws IOException, ClassNotFoundException, SQLException, JSONException{
			
			ManageBoardActions manageBoardActions = new ManageBoardActions();
			BoardResponse(manageBoardActions.addboard(this));
				
		}
		
		//updateboard method for update a particular board into database.
		public void updateBoard() throws IOException, ClassNotFoundException, SQLException, JSONException{
			
			ManageBoardActions manageBoardActions = new ManageBoardActions();
			BoardResponse(manageBoardActions.updateboard(this));
		   
		}
		
		//deleteboard method for delete a particular board into database.
		public void deleteBoard() throws IOException, ClassNotFoundException, SQLException, JSONException{

			ManageBoardActions manageBoardActions = new ManageBoardActions();
			BoardResponse(manageBoardActions.deleteboard(this));
		 
		}
		
		
		//BoardResponse method for creating board object.
		public void BoardResponse(Board board) throws IOException, JSONException {
			
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
		    PrintWriter writer = response.getWriter();
			
		    //BoardObject for storing the board details.
			JSONObject BoardObject = new JSONObject();
		
			BoardObject.put(Constants.STATUS, board.getStatus());
			BoardObject.put("boardId", board.getBoardid());
			BoardObject.put("boardName",board.getBoardname() );
			BoardObject.put("boardTime", board.getBoardtime());
			
			
			writer.write(BoardObject.toString());
			writer.flush();
			writer.close();
		    
			
		}
		
}
