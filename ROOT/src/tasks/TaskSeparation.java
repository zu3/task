package tasks;
import org.apache.struts2.ServletActionContext;
import utility.Constants;
import utility.ExecptionHandling;

public class TaskSeparation {
	
	String taskId;
	String taskName;
	String taskDescription;
	String taskStatus;
	
	public void taskSeparation(){
		
		try {
			
			String method = ServletActionContext.getRequest().getMethod();
			
		    Tasks task = new Tasks();
		   
		    switch (method) {
		    
			case Constants.POST:
	
				task.setTaskName(taskName);
				task.setTaskDescription(taskDescription);
				task.setTaskStatus(taskStatus);
				task.addTask();
				
				break;
				
			case Constants.PUT:
				
				task.setTaskId(taskId);
				task.setTaskName(taskName);
				task.setTaskDescription(taskDescription);
				task.setTaskStatus(taskStatus);
				task.updateTask();
				
				break;
			case Constants.GET:
				    
				task.listTasks();
			
				break;
				
			case Constants.DELETE:
				
				task.setTaskId(taskId);
				task.deleteTask();
				
				break;
			
			default:
				
				ExecptionHandling.exceptionHandling();
				break;
			}

		} catch (Exception e) {
			
			ExecptionHandling.exceptionHandling();
		}
		
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	public String getTaskName() {
		return taskName;
	}


	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}


	public String getTaskDescription() {
		return taskDescription;
	}


	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}


	public String getTaskStatus() {
		return taskStatus;
	}


	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}
	

}