package tasks;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.ServletActionContext;
import dbActions.ManageTaskAction;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import utility.Constants;

public class Tasks{
	
	String taskId;
	String taskName;
	String taskDescription;
	String taskStatus;
	String status;

	public void addTask() throws IOException, ClassNotFoundException, SQLException, JSONException{
		
		ManageTaskAction manageTaskAction = new ManageTaskAction();
		JsonObjectResponse(manageTaskAction.addTask(this));
			
	}
	
	public void updateTask() throws IOException, ClassNotFoundException, SQLException, JSONException{
		
		ManageTaskAction manageTaskAction = new ManageTaskAction();
		JsonObjectResponse(manageTaskAction.Updatetask(this));
	   
	}

	public void deleteTask() throws IOException, ClassNotFoundException, SQLException, JSONException{

		ManageTaskAction manageTaskAction = new ManageTaskAction();
		JsonObjectResponse( manageTaskAction.Deletetask(this));
	 
	}
	
	public void listTasks() throws IOException, ClassNotFoundException, SQLException, JSONException{
	
		ManageTaskAction manageTaskAction = new ManageTaskAction();
		JsonArrayResponse(manageTaskAction.ListTasks());
		
	}
	
	
	public void JsonObjectResponse(Tasks task) throws IOException, JSONException {
		
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
	    PrintWriter writer = response.getWriter();
		
		JSONObject TaskResponse = new JSONObject();
	
		TaskResponse.put(Constants.STATUS, task.getStatus());
		TaskResponse.put("taskId", task.getTaskId());
		TaskResponse.put("taskName", task.getTaskName());
		TaskResponse.put("taskDescription", task.getTaskDescription());
		TaskResponse.put("taskStatus", task.getTaskStatus());
		
		writer.write(TaskResponse.toString());
		writer.flush();
		writer.close();
	    
		
	}
	
	public void JsonArrayResponse(List<Tasks> list) throws IOException, JSONException {
		
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
	    PrintWriter writer = response.getWriter();
		JSONObject TaskResponseObject = new JSONObject();
		JSONArray TaskResponseArray = new JSONArray();
		
		for (int i=0; i<list.size(); i++){
			
			TaskResponseObject.put("taskId", list.get(i).getTaskId());
			TaskResponseObject.put("taskName", list.get(i).getTaskName());
			TaskResponseObject.put("taskDescription", list.get(i).getTaskDescription());
			TaskResponseObject.put("taskStatus", list.get(i).getTaskStatus());
		    
			TaskResponseArray.put(TaskResponseObject);
		}
		
		writer.write(TaskResponseArray.toString());
		writer.flush();
		writer.close();
	    	
	}
	
	
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getTaskDescription() {
		return taskDescription;
	}
	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}
	public String getTaskStatus() {
		return taskStatus;
	}
	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	
}