package dbActions;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import board.Board;
import utility.Constants;

public class ManageBoardActions {

	public Board addboard(Board board) throws ClassNotFoundException, SQLException {
		
		String url = "jdbc:mysql://localhost:3306/TaskManagement";
		String dbuser = "root";
		String dbpass = "";
		
		Board boardResponse = new Board();
		
		DateTimeFormatter date = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		LocalDateTime now = LocalDateTime.now();  
		
		Class.forName("com.mysql.cj.jdbc.Driver");
	    Connection dataConnection = DriverManager.getConnection(url, dbuser, dbpass);
	    
	    PreparedStatement createBoard = dataConnection.prepareStatement("insert into BoardDetails (boardname,boardtime)values(?,?)");
	    createBoard.setString(1, board.getBoardname());
	    createBoard.setString(2, date.format(now));
	    createBoard.executeUpdate();
	    
	    PreparedStatement FetchBoardId = dataConnection.prepareStatement("SELECT max(boardid) as boardid FROM BoardDetails");
	    ResultSet FetchBoardIdResult = FetchBoardId.executeQuery();
	    while (FetchBoardIdResult.next()) {
	    	
	    	boardResponse.setBoardid(String.valueOf(FetchBoardIdResult.getInt("boardid")));
	    }
	    
	    boardResponse.setStatus(Constants.SUCCESS);
		boardResponse.setBoardname(board.getBoardname());
		boardResponse.setBoardtime( date.format(now));
		
		dataConnection.close();
		
		return boardResponse;
	}

	public Board updateboard(Board board) throws SQLException, ClassNotFoundException {
		
		String url = "jdbc:mysql://localhost:3306/TaskManagement";
		String dbuser = "root";
		String dbpass = "";
		
		Board boardResponse = new Board();
		
		Class.forName("com.mysql.cj.jdbc.Driver");
	    Connection dataConnection = DriverManager.getConnection(url, dbuser, dbpass);
	    
	    PreparedStatement UpdateBoard = dataConnection.prepareStatement("update BoardDetails set boardname = ? where boardid =?");
	    UpdateBoard.setString(1, board.getBoardname());
	    UpdateBoard.setInt(2, Integer.parseInt(board.getBoardid()));
	    UpdateBoard.executeUpdate();
	    
	    boardResponse.setStatus(Constants.SUCCESS);
	    boardResponse.setBoardid(board.getBoardid());
	    boardResponse.setBoardname(board.getBoardname());
	    
	    dataConnection.close();
		
		return boardResponse;
	}

	public Board deleteboard(Board board) throws SQLException, ClassNotFoundException {
		
		String url = "jdbc:mysql://localhost:3306/TaskManagement";
		String dbuser = "root";
		String dbpass = "";
		
		Board boardResponse = new Board();
		
		Class.forName("com.mysql.cj.jdbc.Driver");
	    Connection dataConnection = DriverManager.getConnection(url, dbuser, dbpass);
	    
	    PreparedStatement DeleteBoard = dataConnection.prepareStatement("delete from BoardDetails where boardid =?");
	    DeleteBoard.setInt(1, Integer.parseInt(board.getBoardid()));
	    DeleteBoard.executeUpdate();
	    
	    boardResponse.setStatus(Constants.SUCCESS);
	    
	    dataConnection.close();
		
		return boardResponse;
	}
	
	
	
	

}
