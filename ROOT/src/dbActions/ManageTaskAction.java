package dbActions;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import tasks.Tasks;
public class ManageTaskAction {
	
	//Listtask method for add the task
	public Tasks addTask(Tasks task)throws ClassNotFoundException, SQLException{	
		
		String url = "jdbc:mysql://localhost:3306/TaskManagement";
		String dbuser = "root";
		String dbpass = "";
		
		//handling the classnotfound exception
		try {				
			Class.forName("com.mysql.cj.jdbc.Driver");				
		}
		
		catch(ClassNotFoundException e){
			throw new ClassNotFoundException("ClassNotFoundException: "+e.getMessage());
		}
		
		//query for insert the task details.
		String query="insert into TaskDetails (taskname,taskdescription,taskdate,taskstatus) values(?,?,?,?);";
		
		//handling the SQLExeception.
		try(Connection con =  DriverManager.getConnection(url, dbuser, dbpass);PreparedStatement pps=con.prepareStatement(query)){	
			
			//taskname get from task object.
			pps.setString(1,task.getTaskName());
			
			//taskdescription get from task object.
			pps.setString(2,task.getTaskDescription());	
			
			//get the system current time and date.
			DateTimeFormatter dateformat = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
			LocalDateTime now = LocalDateTime.now(); 
			
			//storing the currentdate and time into variable. 
			String date=dateformat.format(now);
			
			pps.setString(3,date);
			
			//set the status as assigned.
			pps.setString(4,"assigned");	
			
			pps.executeUpdate();
			
			//getting the taskid of newly added task in database.
			String query1="select taskid from TaskDetails where taskname=?";
			PreparedStatement pps1=con.prepareStatement(query1);				
			pps1.setString(1,task.getTaskName());
			
			ResultSet rs=pps1.executeQuery();
			
			 //task class object creation.
			 Tasks Task = new Tasks();
			 
			 //get the cursor to result set first row.
		  	 rs.first();
		  	 
			 Task.setTaskId(rs.getString(1));
			 Task.setTaskName(task.getTaskName());
			 Task.setTaskDescription(task.getTaskDescription());
			 Task.setTaskStatus("assigned");	
			 Task.setStatus("success");
			 
			 return Task;
			
		}
		catch(SQLException e){
			throw new SQLException("SqlException: "+e.getMessage());
	    }		

	}
	
	 //Listtask method for update the task.
	 public Tasks Updatetask(Tasks task) throws ClassNotFoundException,SQLException{
					
				String url = "jdbc:mysql://localhost:3306/TaskManagement";
				String dbuser = "root";
				String dbpass = "";
				
				//query for update the task status.
				String query="UPDATE TaskDetails SET taskname=?,taskdescription=?,taskstatus=? WHERE taskid=?";
				
				//handling the classnotfound exception
				try {				
					Class.forName("com.mysql.cj.jdbc.Driver");				
				}
				
				catch(ClassNotFoundException e){
					throw new ClassNotFoundException("ClassNotFoundException: "+e.getMessage());
				}
							
				//handling the SQLExeception.
				try(Connection con =  DriverManager.getConnection(url, dbuser, dbpass);PreparedStatement pps=con.prepareStatement(query)){
					
					//taskname get from task object.
					pps.setString(1,task.getTaskName());
					
					//taskdescription get from task object.
					pps.setString(2,task.getTaskDescription());
					
					//taskstatus get from task object.
					pps.setString(3,task.getTaskStatus());
					
					//get the taskid from task object
					pps.setString(4,task.getTaskId());
					
					pps.executeUpdate();
					
					//task class object creation.
					Tasks Task = new Tasks();
					
					Task.setTaskId(task.getTaskId());
					Task.setTaskName(task.getTaskName());
					Task.setTaskDescription(task.getTaskDescription());
					Task.setTaskStatus(task.getTaskStatus());	
					Task.setStatus("success");
					 
					return Task;
					
				}
				catch(SQLException e){
					throw new SQLException("SqlException: "+e.getMessage());
			    }									
		}	
	
		
		//Listtask method for delete the task.
		public Tasks Deletetask(Tasks task) throws ClassNotFoundException, SQLException{
			
			String url = "jdbc:mysql://localhost:3306/TaskManagement";
			String dbuser = "root";
			String dbpass = "";
			
			//handling the classnotfoundexception
			try {				
				Class.forName("com.mysql.cj.jdbc.Driver");				
			}
			
			catch (ClassNotFoundException e) {
				throw new ClassNotFoundException("ClassNotFoundException: "+e.getMessage());
			}
			
			//query for update the task status.
			String query="delete from TaskDetails where taskid =?";
	    
			//handling the SQLExeception.
			try(Connection con =  DriverManager.getConnection(url, dbuser, dbpass);PreparedStatement pps=con.prepareStatement(query)){	
				
				//get the taskid from task object
				pps.setString(1,task.getTaskId());
				
				pps.executeUpdate();
				
				//task class object creation.
				Tasks Task = new Tasks();
				
				Task.setStatus("success");
				
				return Task;
				
			}
			catch(SQLException e){		
				throw new SQLException("SqlException: "+e.getMessage());
		    }					
			
		}
		
		
		//Listtask method for list the task.
	    public List<Tasks> ListTasks() throws ClassNotFoundException, SQLException{
						  	
			String url = "jdbc:mysql://localhost:3306/TaskManagement";
			String dbuser = "root";
			String dbpass = "";	 
		   
			//handling the classnotfound exception
			try {				
				Class.forName("com.mysql.cj.jdbc.Driver");				
			}
			
			catch (ClassNotFoundException e) {
				throw new ClassNotFoundException("ClassNotFoundException: "+e.getMessage());
			}
			
			//query for insert the task details.
			String query="select taskid,taskname,taskdescription,taskstatus from TaskDetails;";
			
			//Tasks list for storing the task details.	
			try(Connection con =  DriverManager.getConnection(url, dbuser, dbpass);Statement st = con.createStatement();ResultSet rs=st.executeQuery(query)){
					
				//storing the all tasks details.
				List<Tasks> Tasks=new ArrayList<Tasks>();
				
				//storing the data of tasks.
				Tasks Task = new Tasks();
				
				  while(rs.next()){				
					    Task.setTaskId(rs.getString(1));
						Task.setTaskName(rs.getString(2));
						Task.setTaskDescription(rs.getString(3));
						Task.setTaskStatus(rs.getString(4));	
						Tasks.add(Task);	
				  }	
				  
				  return Tasks;	  
			}	
			
			catch(SQLException e){
				
				throw new SQLException("SqlException: "+e.getMessage());
				
		    }	
				
		}	
		
}